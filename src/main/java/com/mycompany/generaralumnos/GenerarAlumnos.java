/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package com.mycompany.generaralumnos;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;
import javax.swing.JFileChooser;

/**
 *
 * @author Razzor
 */
public class GenerarAlumnos extends javax.swing.JFrame {

    /**
     * Creates new form GenerarAlumnos
     */
    public GenerarAlumnos() {
        initComponents();
    }
    String[] PRIMER_APELLIDO = {
        "Garcia",
        "Rodriguez",
        "Martinez",
        "Lopez",
        "Gonzalez",
        "Perez",
        "Sanchez",
        "Romero",
        "Flores",
        "Torres",
        "Ramirez",
        "Cruz",
        "Herrera",
        "Ortiz",
        "Morales",
        "Silva",
        "Castro",
        "Ruiz",
        "Vargas",
        "Jimenez",
        "Reyes",
        "Medina",
        "Morales",
        "Aguilar",
        "Rios",
        "Mendoza",
        "Valdez",
        "Ramos",
        "Torres",
        "Castillo",
        "Gomez",
        "Pineda",
        "Escobar",
        "Montoya",
        "Cordero",
        "Vargas",
        "Velasquez",
        "Zúniga",
        "Chavez",
        "Lopez",
        "Morales",
        "Mendez",
        "Rivas",
        "Delgado",
        "Rosales",
        "Soto",
        "Acosta",
        "Rivera",
        "Guzman",
        "Navarro",
        "Rios",
        "Paredes",
        "Quiroz",
        "Solis",
        "Duarte",
        "Caceres",
        "Gutierrez",
        "Figueroa",
        "Maldonado",
        "Pena",
        "Salazar",
        "Ochoa",
        "Contreras",
        "Fuentes",
        "Palacios",
        "Montenegro",
        "Briceno",
        "Fernandez",
        "Morales",
        "Aguirre",
        "Acevedo",
        "Chavarria",
        "Benavides",
        "Araya",
        "Cespedes"
    };
    
    String[] SEGUNDO_APELLIDO = {
        "Juan",
        "Maria",
        "Jose",
        "Guadalupe",
        "Francisco",
        "Ana",
        "Pedro",
        "Luis",
        "Carlos",
        "Miguel",
        "Rosa",
        "Jesús",
        "Antonio",
        "Fernanda",
        "Manuel",
        "Laura",
        "Alejandro",
        "Javier",
        "Diego",
        "Veronica",
        "Roberto",
        "Ricardo",
        "Sofia",
        "Gonzalo",
        "Carmen",
        "Mauricio",
        "Patricia",
        "Isabella",
        "Andres",
        "Ana Maria",
        "Felipe",
        "Eduardo",
        "Fatima",
        "Alberto",
        "Adriana",
        "Emilio",
        "Daniela",
        "Rodrigo",
        "Esteban",
        "Valeria",
        "Julio",
        "Gabriela",
        "angel",
        "Martha",
        "Hugo",
        "Isabel",
        "Ruben",
        "Victoria",
        "Fernando",
        "Cecilia",
        "Arturo",
        "Natalia",
        "Victor",
        "Paulina",
        "Jorge",
        "Ingrid",
        "Jaime",
        "Daniella",
        "Raúl",
        "Miranda",
        "Erick",
        "Ivonne",
        "Gerardo",
        "Michelle",
        "Leonardo",
        "Valentina",
        "Rafael",
        "Renata",
        "Mariano",
        "Ximena",
        "Ignacio",
        "Brenda",
        "Martin",
        "Abril",
        "Mario",
        "Silvia",
        "Samuel",
        "Lorena",
        "Juan Carlos",
        "Ana Laura",
        "Luis Miguel",
        "Alicia"

    };
    
    String[] PRIMER_NOMBRE = {
        "Smith",
        "Johnson",
        "Williams",
        "Brown",
        "Jones",
        "Miller",
        "Davis",
        "Taylor",
        "Wilson",
        "Clark",
        "Lewis",
        "Hall",
        "Allen",
        "Young",
        "Walker",
        "Lewis",
        "Scott",
        "Adams",
        "Baker",
        "Mitchell",
        "Carter",
        "Roberts",
        "Turner",
        "Phillips",
        "King",
        "Hill",
        "Wright",
        "Morris",
        "Watson",
        "Cooper",
        "Perry",
        "Coleman",
        "Stewart",
        "Morgan",
        "Reed",
        "Butler",
        "Sanders",
        "Hayes",
        "Foster",
        "Hudson",
        "Bryant",
        "Reynolds",
        "Bailey",
        "Bell",
        "Sullivan",
        "Russell",
        "Griffin",
        "Diaz",
        "Haynes",
        "Gordon",
        "Douglas",
        "Arnold",
        "Weaver",
        "Sims",
        "Banks",
        "Kennedy",
        "Morrison",
        "Francis",
        "Stevens",
        "Fisher",
        "Jenkins",
        "Hawkins",
        "Harper",
        "Chandler",
        "Lawrence",
        "Garner",
        "Andrews",
        "Washington",
        "Greene",
        "Ford",
        "Pearson",
        "Holland",
        "Manning",
        "Craig",
        "Spencer",
        "Bishop",
        "Sutton",
        "Peters",
        "Stephens",
        "Hunt",
        "Black",
        "Ferguson",
        "Mason",
        "Miller",
        "Reid",
        "Holmes"
    };
    
    String[] SEGUNDO_NOMBRE = {
        "Smith",
        "Johnson",
        "Williams",
        "Brown",
        "Jones",
        "Miller",
        "Davis",
        "Taylor",
        "Wilson",
        "Clark",
        "Lewis",
        "Hall",
        "Allen",
        "Young",
        "Walker",
        "Lewis",
        "Scott",
        "Adams",
        "Baker",
        "Mitchell",
        "Carter",
        "Roberts",
        "Turner",
        "Phillips",
        "King",
        "Hill",
        "Wright",
        "Morris",
        "Watson",
        "Cooper",
        "Perry",
        "Coleman",
        "Stewart",
        "Morgan",
        "Reed",
        "Butler",
        "Sanders",
        "Hayes",
        "Foster",
        "Hudson",
        "Bryant",
        "Reynolds",
        "Bailey",
        "Bell",
        "Sullivan",
        "Russell",
        "Griffin",
        "Diaz",
        "Haynes",
        "Gordon",
        "Douglas",
        "Arnold",
        "Weaver",
        "Sims",
        "Banks",
        "Kennedy",
        "Morrison",
        "Francis",
        "Stevens",
        "Fisher",
        "Jenkins",
        "Hawkins",
        "Harper",
        "Chandler",
        "Lawrence",
        "Garner",
        "Andrews",
        "Washington",
        "Greene",
        "Ford",
        "Pearson",
        "Holland",
        "Manning",
        "Craig",
        "Spencer",
        "Bishop",
        "Sutton",
        "Peters",
        "Stephens",
        "Hunt",
        "Black",
        "Ferguson",
        "Mason",
        "Miller",
        "Reid",
        "Holmes"
    };




    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGenerarAlumnos = new javax.swing.JButton();
        buttonGenerarSql = new javax.swing.JButton();
        buttonGenerarCsv = new javax.swing.JButton();
        buttonExportar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        resultadoTextArea = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        buttonGenerarAlumnos.setText("<html> Generar <br> Alumnos </html>");
        buttonGenerarAlumnos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonGenerarAlumnosActionPerformed(evt);
            }
        });

        buttonGenerarSql.setText("Generar SQL");
        buttonGenerarSql.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonGenerarSqlActionPerformed(evt);
            }
        });

        buttonGenerarCsv.setText("Generar CSV");
        buttonGenerarCsv.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonGenerarCsvActionPerformed(evt);
            }
        });

        buttonExportar.setText("Exportar");

        resultadoTextArea.setColumns(20);
        resultadoTextArea.setRows(5);
        jScrollPane1.setViewportView(resultadoTextArea);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(buttonGenerarAlumnos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(buttonGenerarSql)
                        .addGap(18, 18, 18)
                        .addComponent(buttonGenerarCsv)
                        .addGap(18, 18, 18)
                        .addComponent(buttonExportar, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 180, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(buttonGenerarAlumnos, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonGenerarSql, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(buttonGenerarCsv, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(buttonExportar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 381, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void buttonGenerarAlumnosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonGenerarAlumnosActionPerformed
        // TODO add your handling code here:
        generar();
    }//GEN-LAST:event_buttonGenerarAlumnosActionPerformed

    private void generar() {
        StringBuilder salida = new StringBuilder();
        salida.append("DROP DATABASE IF EXISTS sistema_escolar;\n");
        salida.append("CREATE DATABASE IF NOT EXISTS sistema_escolar;\n");
        salida.append("INSERT INTO sistema_escolar.alumnos(apellido, nombre, nombre_completo, correo_electronico, telefono) VALUES\n");
        Random random = new Random();

        for (int i = 0; i < 125001; i++) {
            salida.append(String.format("('%s', '%s', '%s %s', '202356789%08d@uthermosillo.edu.mx', '662%07d'), %n",
                    PRIMER_APELLIDO[random.nextInt(PRIMER_APELLIDO.length)],
                    SEGUNDO_APELLIDO[random.nextInt(SEGUNDO_APELLIDO.length)],
                    PRIMER_NOMBRE[random.nextInt(PRIMER_NOMBRE.length)],
                    SEGUNDO_NOMBRE[random.nextInt(SEGUNDO_NOMBRE.length)],
                    i, random.nextInt((int) Math.pow(10, 7))));
            if (i == 125001) {
                salida.append(salida).append(";%n");
            }
        }
        resultadoTextArea.setText(salida.toString());
    }
    
    private void buttonGenerarSqlActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonGenerarSqlActionPerformed
        generarSql();
    }//GEN-LAST:event_buttonGenerarSqlActionPerformed

    private void generarSql() {
        String contenidoSinBR = resultadoTextArea.getText().replaceAll("<br>", "\n");

        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setSelectedFile(new File("alumnos.sql"));

        int returnValue = fileChooser.showSaveDialog(this);

        if (returnValue == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();

            try ( PrintWriter writer = new PrintWriter(file, "UTF-8")) {
                writer.print(contenidoSinBR);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    private void buttonGenerarCsvActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonGenerarCsvActionPerformed
        generarCsv();
    }//GEN-LAST:event_buttonGenerarCsvActionPerformed

    private void generarCsv() {
        StringBuilder contenidoCsv = new StringBuilder();
        contenidoCsv.append("PrimerApellido,SegundoApellido,NombreCompleto,Email,Telefono\n");

        Random random = new Random();

        for (int i = 0; i < 125001; i++) {
            String lineaCsv = String.format("\"%s\",\"%s\",\"%s %s\",\"%s\",\"662%d\"\n",
                    PRIMER_APELLIDO[random.nextInt(PRIMER_APELLIDO.length)],
                    SEGUNDO_APELLIDO[random.nextInt(SEGUNDO_APELLIDO.length)],
                    PRIMER_NOMBRE[random.nextInt(PRIMER_NOMBRE.length)],
                    SEGUNDO_NOMBRE[random.nextInt(SEGUNDO_NOMBRE.length)],
                    "202356789" + i + "@uthermosillo.edu.mx",
                    random.nextInt((int) Math.pow(10, 7)));
            contenidoCsv.append(lineaCsv);
        }

        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setSelectedFile(new File("alumnos.csv"));

        int returnValue = fileChooser.showSaveDialog(this);

        if (returnValue == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();

            try ( PrintWriter writer = new PrintWriter(file, "UTF-8")) {
                writer.print(contenidoCsv.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GenerarAlumnos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GenerarAlumnos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GenerarAlumnos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GenerarAlumnos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GenerarAlumnos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonExportar;
    private javax.swing.JButton buttonGenerarAlumnos;
    private javax.swing.JButton buttonGenerarCsv;
    private javax.swing.JButton buttonGenerarSql;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea resultadoTextArea;
    // End of variables declaration//GEN-END:variables
}
